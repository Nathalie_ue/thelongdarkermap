﻿using UnityEngine;
using System.Collections;

public class DeactivateFrost : MonoBehaviour
{
    public Camera mainCamera;
    public FrostEffect frostEffect;

    private void Start()
    {
        frostEffect = mainCamera.GetComponent<FrostEffect>();
    }

    private void OnTriggerEnter(Collider other)
    {
        frostEffect.enabled = false;
    }
}
