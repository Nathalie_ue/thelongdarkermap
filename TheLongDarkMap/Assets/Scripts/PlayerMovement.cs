﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    //Script für die Bewegung des Spielers
    public class PlayerMovement : MonoBehaviour
    {
        //CharakterController, auf den sich bezogen wird
        private CharacterController characterController;

        //Um den CharacterController zu bewegen
        private Vector3 motion;
        
        //Die Gravitation des Spielers
        private Vector3 gravity; 
        
        //Die Stärke der Gravitation
        [SerializeField]
        private float gravityValue = -9.81f;

        //Die Geschwindigkeit des Spielers
        [SerializeField]
        private float walkSpeed = 5f;
        
        
        private void Awake()
        {
            //Das CharacterController Component wird in der Variabel zwischengespeichert
            characterController = GetComponent<CharacterController>();            
        }

        private void Update()
        {
            //Der Wert der horizontalen (A/D & Left/Right Keys) und 
            //verticalen(W/S & Up/Down Keys) Achse wird in variabeln zwischengespeichert
            float horizontalInput = Input.GetAxis("Horizontal");
            float verticalInput = Input.GetAxis("Vertical");

            //Um sich in die Richtung zu bewegen, in die der Spieler schaut
            motion = transform.right * horizontalInput + transform.forward * verticalInput;

            //Die Bewegung wird in die Move Methode des CharacterContoller übertragen
            //und an die framerate und die von uns gegebene Geschwindigkeit angepasst
            characterController.Move(motion * walkSpeed * Time.deltaTime);
            
            //Die Gravitation wird auf die y Achse gelegt
            gravity.y = gravityValue;

            //Die Gravitation wird auf die Move Methode des CharacterControllers
            //übertragen und an die framerate angepasst
            characterController.Move(gravity * Time.deltaTime);

            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
    }
