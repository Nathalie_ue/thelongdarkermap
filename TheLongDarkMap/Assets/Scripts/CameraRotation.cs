﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

    //Script für die Kamerabewegung
    public class CameraRotation : MonoBehaviour
    {
        //Wie empfindlich die Mausbewegung sein soll
        [SerializeField]
        private float mouseSensitivity = 5f;

        //Position des Player Objekt
        private Transform playerTransform;

        //pitch axis rotation
        private float pitchAxis;

        //Kamera, auf die sich bezogen wird
        private Camera mainCamera;
        
        private void Start()
        {
            //Damit sich die Kamera mit dem Körper des Spielers dreht
            playerTransform = transform.parent;
        }

        private void Update()
        {
            //"Mouse X" ist die horizontal axis der Maus - Siehe Project Input Settings
            //"Mouse Y" ist die vertical axis der Maus - Siehe Project Input Settings
            float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity;
            
            //Bewegung auf der Y achse
            pitchAxis -= mouseY;
            
            //Begrenzung wie weit nach oben/unter man schauen kann
            pitchAxis = Mathf.Clamp(pitchAxis, -45f, 45f);

            //Rotiert die Kamera
            transform.localRotation = Quaternion.Euler(pitchAxis, 0f, 0f);

            //Rotiert playerTransform in bezug auf den mouseX input
            playerTransform.Rotate(0f, mouseX, 0f);            
        }
    }

